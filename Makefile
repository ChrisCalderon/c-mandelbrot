SHELL = /bin/bash
CC = $(shell ./find-clang.sh)

ifeq (.SHELLSTATUS, 0)
$(error No suitable compiler found!)
endif

CFLAGS = -std=c99 -Wall -Werror -pedantic -pthread -c -O2
OBJECTS = main.o cli.o simple.o
LDLIBS = -lpng -lm -lc
LINK = $(CC) $(LDLIBS)
COMPILE = $(CC) $(CFLAGS)

UNAME := $(shell uname)
ifeq ($(UNAME), Linux)
COMPILE += -D_POSIX_C_SOURCE=200112L
else ifeq ($(UNAME), Darwin)
COMPILE += -I/opt/local/include
LINK += -L/opt/local/lib
endif


.PHONY: clean set-debug-flag all debug

all: mandelbrot

debug: clean set-debug-flag mandelbrot

mandelbrot: $(OBJECTS)
	$(LINK) -o mandelbrot $(OBJECTS)

main.o: main.c *.h
	$(COMPILE) main.c

%.o: %.c %.h
	$(COMPILE) $<

clean:
	if [ -x mandelbrot ]; then rm mandelbrot; fi;
	for file in $(OBJECTS); do if [ -f $$file ]; then rm $$file; fi; done;

set-debug-flag:
	$(eval CFLAGS += -g)
