/*
    c-mandelbrot is software that makes simple images of the Mandelbrot set.
    Copyright (C) 2021 Chris Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cli.h"

static void usage() {
    printf("Usage: %s [OPTION]...\n", PROG);
    printf("Generate a black and white image of the mandelbrot set.\n\n"
    "These are the recognized options:\n\n"
    "  -h --help         Show this help and exit.\n\n"
    "  -o --output       Set the name of the created image file.\n"
    "                    The default is mandelbrot-wb.png.\n\n"
    "  -W --pixel-width  Set the width of the image. The default is 10000.\n\n"
    "  -H --pixel-height Set the height of the image. The default is 10000.\n\n"
    "  -w --real-width   Set the width of the real axis. This defines the\n"
    "                    difference between the minimum and maximum real\n"
    "                    parts used to generate the image. This is in turn\n"
    "                    used to define the distance between the minimum\n"
    "                    and maximum imaginary parts and the distance\n"
    "                    between adjacent pixels. The default is 3.0.\n\n"
    "  -I --center-imag  Set the imaginary part of the center of the image.\n"
    "                    The default is 0.0.\n\n"
    "  -R --center-real  Set the real part of the center of the image.\n"
    "                    The default is -0.75.\n\n"
    "  -T --max-trials   Set the maximum number of iterations to use to\n"
    "                    check if a point is in the Mandelbrot set.\n"
    "                    The default is 10000.\n\n"
    "  -s --style        Set the style used to generate the image.\n"
    "                    The options are simple or banded. The simple style produces\n"
    "                    a black an white, 1-bit image. The banded style produces\n"
    "                    a colored image in which regions with the same escape count\n"
    "                    are colored the same, using a PNG color palette. Up to 256\n"
    "                    colors may be used in a banded image.\n"
    "                    The default is simple.\n\n"
    " -p --palette       Set the file to read the palette from.\n"
    "                    Palette colors should be in hex format in a text file with\n"
    "                    each color on a separate line. A maximum of 256 colors are\n"
    "                    allowed. If this option isn't present and the style is set\n"
    "                    to banded, then a random palette will be generated and\n"
    "                    saved to palette.txt.\n\n");
}


int parse_options(int argc, char **argv, option_namespace *namespace) {
    static struct option longopts[] = {
            {"help", no_argument, NULL, 'h'},
            {"output", required_argument, NULL, 'o'},
            {"pixel-width", required_argument, NULL, 'W'},
            {"pixel-height", required_argument, NULL, 'H'},
            {"real-width", required_argument, NULL, 'w'},
            {"center-imag", required_argument, NULL, 'I'},
            {"center-real", required_argument, NULL, 'R'},
            {"max-trials", required_argument, NULL, 'T'},
            {"style", required_argument, NULL, 's'},
            {"palette", required_argument, NULL, 'p'},
            {NULL, 0, NULL, 0}
    };

    int ch;
    while((ch = getopt_long(argc, argv, "ho:W:H:w:I:R:T:", longopts, NULL)) != -1){
        switch(ch){
            case 'h':
                usage();
                return -1;

            case 'o':
                namespace->image_name = optarg;
                break;

            case 'W':
                namespace->pixel_width = strtoul(optarg, NULL, 10);
                if(errno == ERANGE){
                    fputs("Width option value is out of range.\n", stderr);
                    return EX_USAGE;
                }
                break;

            case 'H':
                namespace->pixel_height = strtoul(optarg, NULL, 10);
                if(errno == ERANGE){
                    fputs("Height option value is out of range.\n", stderr);
                    return EX_USAGE;
                }
                break;

            case 'w':
                namespace->real_width = strtod(optarg, NULL);
                if(errno == ERANGE){
                    fputs("Real width option value is out of range.\n", stderr);
                    return EX_USAGE;
                }
                break;

            case 'I':
                namespace->center_imag = strtod(optarg, NULL);
                if(errno == ERANGE){
                    fputs("The imaginary part of the image center is out of range.\n", stderr);
                    return EX_USAGE;
                }
                break;

            case 'R':
                namespace->center_real = strtod(optarg, NULL);
                if(errno == ERANGE){
                    fputs("The real part of the image center is out of range.\n", stderr);
                    return EX_USAGE;
                }
                break;

            case 'T':
                namespace->max_trials = strtoul(optarg, NULL, 10);
                if(errno == ERANGE){
                    fputs("The maximum number of trials is out of range.\n", stderr);
                    return EX_USAGE;
                }
                break;

            case 's':
                if(strcmp(optarg, "simple") == 0)
                    namespace->image_style = SIMPLE;
                else if (strcmp(optarg, "banded") == 0)
                    namespace->image_style = BANDED;
                else {
                    fprintf(stderr, "Unrecognized value for style option: %s\n", optarg);
                    return EX_USAGE;
                }
                break;

            case 'p':
                namespace->palette_name = optarg;
                break;

            case '?':
            default:
                fprintf(stderr, "Unknown option character encountered: %c\n", (char)optopt);
                return EX_USAGE;
        }
    }
    return EX_OK;
}

