//
// Created by Christian Calderon on 12/10/21.
//

#ifndef C_MANDELBROT_SIMPLE_H
#define C_MANDELBROT_SIMPLE_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sysexits.h>
#include <complex.h>
#include <errno.h>
#include <pthread.h>
#include <png.h>

int simple_mandelbrot(FILE *image_fp, long cpu_count, double i_max, double r_min, double step, size_t pixel_width, size_t pixel_height, uint64_t max_trials);

#endif //C_MANDELBROT_SIMPLE_H
