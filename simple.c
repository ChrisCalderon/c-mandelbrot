//
// Created by Christian Calderon on 12/10/21.
//

#include "simple.h"

struct worker_args {
    pthread_cond_t *wait_cond;
    pthread_mutex_t *wait_mut;
    bool *wait;
    double i_max;
    double r_min;
    double real_step;
    size_t row_start;
    size_t row_step;
    size_t pixel_width;
    size_t pixel_height;
    uint64_t max_trials;
    png_byte **rows;
};


static void *worker(void *args_) {
    struct worker_args *args = args_;

    pthread_mutex_lock(args->wait_mut);
    while(*(args->wait))
        pthread_cond_wait(args->wait_cond, args->wait_mut);
    pthread_mutex_unlock(args->wait_mut);

    double i_max = args->i_max;
    double r_min = args->r_min;
    double real_step = args->real_step;
    size_t pixel_width = args->pixel_width;
    size_t pixel_height = args->pixel_height;
    size_t row_start = args->row_start;
    size_t row_step = args->row_step;
    uint64_t max_trials = args->max_trials;
    png_byte **rows = args->rows;

    for(size_t i = row_start; i < pixel_height; i += row_step) {
        double complex i_imag = (i_max - i*real_step)*I;
        for(size_t r = 0; r < pixel_width; r++) {
            double complex z = r_min + r*real_step + i_imag;
            if(cabs(1.0 - csqrt(1.0 - 4.0*z)) < 1.0) {
                rows[i][r] = 1;
                continue;
            }
            double complex c = z;
            bool is_member = true;
            for(uint64_t trial = 0; trial < max_trials; trial++) {
                z = z*z + c;
                if(cabs(z) > 2.0) {
                    is_member = false;
                    break;
                }
            }
            rows[i][r] = is_member;
        }
    }
    return NULL;
}

int simple_mandelbrot(FILE *image_fp, long cpu_count, double i_max, double r_min, double step, size_t pixel_width, size_t pixel_height, uint64_t max_trials) {
    png_byte *data = malloc(pixel_width * pixel_height * sizeof(png_byte));
    if(data == NULL) {
        fputs("Unable to allocate memory for image!\n", stderr);
        return EX_OSERR;
    }
    png_byte **rows = malloc(pixel_height * sizeof(png_byte*));
    if(rows == NULL) {
        fputs("Unable to allocate memory for image!\n", stderr);
        free(data);
        return EX_OSERR;
    }

    size_t row_length = pixel_width * sizeof(png_byte);
    for(size_t i=0; i<pixel_height; i++)
        rows[i] = data + i*row_length;

    pthread_t *threads = malloc(sizeof(pthread_t) * cpu_count);
    if(threads == NULL) {
        fputs("Unable to allocate memory for threads!\n", stderr);
        free(rows);
        free(data);
        return EX_OSERR;
    }

    struct worker_args *args = malloc(sizeof(struct worker_args) * cpu_count);
    if(args == NULL) {
        fputs("Unable to allocate memory for threads!\n", stderr);
        free(threads);
        free(rows);
        free(data);
        return EX_OSERR;
    }

    pthread_cond_t wait_cond;
    pthread_mutex_t wait_mut;
    bool wait = true;

    if((pthread_cond_init(&wait_cond, NULL) != 0) || (pthread_mutex_init(&wait_mut, NULL) != 0)) {
        perror("Error initializing thread synchronization");
        free(args);
        free(threads);
        free(rows);
        free(data);
        return EX_OSERR;
    }

    for(size_t i=0; i < cpu_count; i++) {
        args[i].wait_cond = &wait_cond;
        args[i].wait_mut = &wait_mut;
        args[i].wait = &wait;
        args[i].i_max = i_max;
        args[i].r_min = r_min;
        args[i].real_step = step;
        args[i].row_start = i;
        args[i].row_step = cpu_count;
        args[i].pixel_width = pixel_width;
        args[i].pixel_height = pixel_height;
        args[i].max_trials = max_trials;
        args[i].rows = rows;

        if(pthread_create(&threads[i], NULL, worker, &args[i]) != 0) {
            perror("Unable to create all threads");
            for(size_t j=0; j<i; j++) {
                pthread_cancel(threads[j]);
                pthread_join(threads[j], NULL);
            }
            free(args);
            free(threads);
            free(rows);
            free(data);
            return EX_OSERR;
        }
    }

    pthread_mutex_lock(&wait_mut);
    wait = false;
    pthread_cond_broadcast(&wait_cond);
    pthread_mutex_unlock(&wait_mut);

    for(size_t i=0; i<cpu_count; i++)
        pthread_join(threads[i], NULL);

    free(args);
    free(threads);

    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
                                                  NULL,
                                                  NULL,
                                                  NULL);
    if(png_ptr == NULL){
        fputs("Unable to create png struct.\n", stderr);
        free(rows);
        free(data);
        return EX_OSERR;
    }

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if(info_ptr == NULL){
        fputs("Unable to create png info struct.\n", stderr);
        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
        free(rows);
        free(data);
        return EX_OSERR;
    }

    if(setjmp(png_jmpbuf(png_ptr))) {
        fputs("There was a libpng related error.\n", stderr);
        png_destroy_write_struct(&png_ptr, &info_ptr);
        free(rows);
        free(data);
        return EX_SOFTWARE;
    }

    png_init_io(png_ptr, image_fp);
    png_set_IHDR(png_ptr,
                 info_ptr,
                 pixel_width,
                 pixel_height,
                 1,
                 PNG_COLOR_TYPE_GRAY,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png_ptr, info_ptr);
    png_set_packing(png_ptr);
    png_write_image(png_ptr, rows);
    png_write_end(png_ptr, info_ptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    free(rows);
    free(data);
    return EX_OK;
}

