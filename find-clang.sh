#!/bin/bash
if [[ $CC == clang* ]]
then
  # Use the CC specified in the environment.
  echo -n "$CC"
  exit 0
fi

if which clang &> /dev/null
then
  echo -n clang
  exit 0
fi

for i in {13..9..-1}
do
  # Some Linux distros have clang installed as clang-$version,
  # while others use clang$version.
  if which "clang$i" &> /dev/null
  then
    echo -n "clang$i"
    exit 0
  fi
  if which "clang-$i" &> /dev/null
  then
    echo -n "clang-$i"
    exit 0
  fi
done
exit 1