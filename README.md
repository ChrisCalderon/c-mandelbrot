![Image of the Mandelbrot set.](mandelbrot-wb.png)
# c-mandelbrot
This software makes simple images of the Mandelbrot set. Right now it only makes 1-bit black and white PNG images.

## Dependencies
* libpng

To install libpng on macOS, use MacPorts: `sudo port selfupdate && sudo port -vN install libpng`

To install libpng on OpenSUSE Tumbleweed: `sudo zypper refresh && sudo zypper install libpng16-16`

## Building
Build the software with the usual commands: `./configure && make`.

These build scripts assume you have clang installed on your system. On macOS this requires installing Xcode and the
Command Line Developer Tools. In OpenSUSE Tumbleweed you have to install clang, lld, and gcc: `sudo zypper install clang-13 lld13 gcc`
The build scripts build an executable named `mandelbrot`. Running this with `-h` or `--help` will show all the options.
When run without options this program will create the image above, a 10k x 10k pixel image. On my 16-core Mac Pro this takes
about 10 seconds, on my 8-core 16 inch MacBook Pro about 20 seconds, on my M1 MBP about 30 seconds, and on my Framework laptop
running OpenSUSE Tumbleweed about a minute.
