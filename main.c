/*
    c-mandelbrot is software that makes simple images of the Mandelbrot set.
    Copyright (C) 2021 Chris Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "cli.h"
#include "simple.h"
#include <stdio.h>
#include <sysexits.h>
#include <png.h>
#include <unistd.h>


int main(int argc, char **argv)
{
  option_namespace namespace = {
          .image_name=NULL,
          .palette_name=NULL,
          .image_style=SIMPLE,
          .pixel_height=10000,
          .pixel_width=10000,
          .max_trials=10000,
          .real_width=3.0,
          .center_imag=0.0,
          .center_real=-0.75
  };

  int result = parse_options(argc, argv, &namespace);
  switch(result){
    case -1: // -1 means help flag was encountered
      return EX_OK;
    case 0: // 0 means all options parsed successfully
      break;
    default: // anything else is an exit code.
      return result;
  }


    
  double real_step = namespace.real_width / namespace.pixel_width;
  double i_max = namespace.center_imag + namespace.pixel_height*real_step/2;
  double r_min = namespace.center_real - namespace.pixel_width*real_step/2;
  long cpu_count = sysconf(_SC_NPROCESSORS_ONLN);


  FILE *image_fp;
  if(namespace.image_name != NULL){
      image_fp = fopen(namespace.image_name, "wb");
  } else {
      image_fp = fopen("mandelbrot.png", "wb");
  }

  if(image_fp == NULL){
    fputs("Unable to open file for image.\n", stderr);
    return EX_CANTCREAT;
  }

  switch(namespace.image_style) {
      case SIMPLE:
          return simple_mandelbrot(image_fp,
                                   cpu_count,
                                   i_max,
                                   r_min,
                                   real_step,
                                   namespace.pixel_width,
                                   namespace.pixel_height,
                                   namespace.max_trials);
      case BANDED:
      default:
          printf("Image style not implemented.\n");
  }

  return EX_OK;
}
