/*
    c-mandelbrot is software that makes simple images of the Mandelbrot set.
    Copyright (C) 2021 Chris Calderon

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef C_MANDELBROT_CLI_H
#define C_MANDELBROT_CLI_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <sysexits.h>
#include <getopt.h>
#include <errno.h>

#ifndef PROG
#define PROG "mandelbrot"
#endif

enum ImageStyle {SIMPLE, BANDED};

typedef struct option_namespace_s {
    char *image_name;
    char *palette_name;
    enum ImageStyle image_style;
    size_t pixel_width;
    size_t pixel_height;
    uint64_t max_trials;
    double real_width;
    double center_imag;
    double center_real;
} option_namespace;

int parse_options(int argc, char **argv, option_namespace *namespace);

#endif //C_MANDELBROT_CLI_H
